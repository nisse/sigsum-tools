module main

go 1.18

require sigsum.org/sigsum-go v0.1.10-0.20221125080842-edb4d038c189

require local/ssh v0.0.0

replace local/ssh => ../ssh

require local/key v0.0.0

replace local/key => ../key
