package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	local_key "local/key"
	"local/ssh"
	"log"
	"net/http"
	"net/url"
	"os"
	"sigsum.org/sigsum-go/pkg/ascii"
	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/key"
	"sigsum.org/sigsum-go/pkg/merkle"
	"sigsum.org/sigsum-go/pkg/requests"
	"sigsum.org/sigsum-go/pkg/types"
	"strings"
	"time"
)

type KeyGenSettings struct {
	outputFile string
	sshFormat  bool
}

type KeyVerifySettings struct {
	keyFile       string
	signatureFile string
	namespace     string
}

type KeySignSettings struct {
	keyFile    string
	outputFile string
	namespace  string
	sshFormat  bool
}

type LeafSettings struct {
	keyFile    string
	logUrl     string
	logKey     string
	outputFile string
}

type VerifySettings struct {
	proofFile string
	submitKey string
	logKey    string
}

const usage = `sigsum is a tool to managed sigsum objects, and interacting with a log server.

Usage:

  sigsum help   Usage message
  sigsum key    Key generation, and low-level sign and verify operations.
  sigsum leaf   Create and/or submit a new leaf to a log.
  sigsum verify Verify a sigsum proof
`

func main() {

	keyCommand := func(args []string) {
		const keyUsage = `sigsum key sub commands:

  sigsum key help 
    Display this help.

  sigsum key gen -o KEY-FILE [--ssh] Generate a new key pair.
    Private key is stored in the given KEY-FILE, hex-encoded.
    Corresponding public key file gets a ".pub" suffix.
    If --ssh option is used, the public file is written in
    OpenSSH format, otherwise raw hex.

  sigsum key verify -k KEY -s SIGNATURE [-n NAMESPACE] < MSG
    KEY and SIGNATURE are file names.
    NAMESPACE is a string, default being "tree_head:v0@sigsum.org"

  sigsum key sign --ssh -k KEY [-n NAMESPACE] [-o SIGNATURE] < MSG
    KEY and SIGNATURE are file names.
    NAMESPACE is a string, default being "tree-leaf:v0@sigsum.org"
    If --ssh is provided, produce an ssh signature file, otherwise raw hex.
`
		if len(args) == 0 {
			log.Printf(keyUsage)
			os.Exit(1)
		}

		cmd, args := args[0], args[1:]
		switch cmd {
		case "help":
			log.Printf(keyUsage)
			os.Exit(0)
		case "gen":
			settings := parseKeyGenSettings(args)
			var priv crypto.PrivateKey
			n, err := rand.Read(priv[:])
			if err != nil {
				log.Fatalf("getting random key failed: %v\n", err)
			}
			if n != crypto.PrivateKeySize {
				log.Fatalf("key generation failed, got only %d out of %d random bytes",
					n, crypto.PrivateKeySize)
			}
			pub := crypto.NewEd25519Signer(&priv).Public()
			writeKeyFile(settings.outputFile, settings.sshFormat,
				&pub, &priv)
		case "verify":
			settings := parseKeyVerifySettings(args)
			publicKey := readPublicKeyFile(settings.keyFile)
			signature := readSignatureFile(settings.signatureFile,
				&publicKey, settings.namespace)
			hash, err := crypto.HashFile(os.Stdin)
			if err != nil {
				log.Fatalf("failed to read stdin: %v\n", err)
			}

			if !crypto.Verify(&publicKey,
				ssh.SignedDataFromHash(settings.namespace, hash),
				&signature) {
				log.Fatalf("signature is not valid\n")
			}
		case "sign":
			settings := parseKeySignSettings(args)
			signer := readPrivateKeyFile(settings.keyFile)
			hash, err := crypto.HashFile(os.Stdin)
			if err != nil {
				log.Fatalf("failed to read stdin: %v\n", err)
			}
			signature, err := signer.Sign(ssh.SignedDataFromHash(settings.namespace, hash))
			if err != nil {
				log.Fatalf("signing failed: %v", err)
			}
			public := signer.Public()
			writeSignatureFile(settings.outputFile, settings.sshFormat,
				&public, settings.namespace, &signature)
		}
	}
	leafCommand := func(args []string) {
		const leafUsage = `sigsum leaf [OPTIONS] < INPUT
    Options:
      -h --help Display this help
      -k PRIVATE-KEY 
      --log-url LOG-URL
      --log-key LOG-KEY

    Creates and/or submits add-leaf request.
    
    If -k PRIVATE-KEY is provided, a new leaf is created based on the
    INPUT message (note that it's size must be exactly 32 octets).

    If -k option is missing, the INPUT should instead be the body of a 
    leaf request, which is parsed and verified.

    If --log-url is provided, the leaf is submitted to the log, and a sigsum
    proof is collected and written to stdout. The log's public key (file)
    must be passed with the --log-key option.

    With -k and no --log-url, leaf request is written to stdout. With no -k and no
    --log-url, just verifies the leaf syntax and signature.
`
		settings := parseLeafSettings(args, leafUsage)
		var leaf requests.Leaf
		if len(settings.keyFile) > 0 {
			signer := readPrivateKeyFile(settings.keyFile)
			publicKey := signer.Public()

			// TODO: Optionally create message by hashing stdin.
			msg := readMessage(os.Stdin)

			signature, err := types.SignLeafMessage(signer, msg[:])
			if err != nil {
				log.Fatalf("signing failed: %v", err)
			}
			leaf = requests.Leaf{Signature: signature, PublicKey: publicKey}
			// TODO: Some impedance mismatch;
			// SignLeafMessage wants message as a []byte,
			// but requests.Leaf.Message is a crypto.Hash.
			copy(leaf.Message[:], msg[:])

			if len(settings.logUrl) == 0 {
				file := os.Stdout
				if len(settings.outputFile) > 0 {
					var err error
					file, err = os.OpenFile(settings.outputFile,
						os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
					if err != nil {
						log.Fatalf("failed to open file '%v': %v", settings.outputFile, err)
					}
					defer file.Close()
				}
				if err := leaf.ToASCII(file); err != nil {
					log.Fatal("writing leaf to stdout failed: %v", err)
				}
				return
			}
		} else {
			if err := leaf.FromASCII(os.Stdin); err != nil {
				log.Fatalf("parsing leaf request failed: %v", err)
			}
			if !types.VerifyLeafMessage(&leaf.PublicKey, leaf.Message[:], &leaf.Signature) {
				log.Fatalf("invalid leaf signature")
			}
		}
		if len(settings.logUrl) > 0 {
			publicKey := readPublicKeyFile(settings.logKey)
			proof, err := submitLeaf(settings.logUrl, &publicKey, &leaf)
			if err != nil {
				log.Fatalf("submitting leaf failed: %v", err)
			}
			file := os.Stdout
			if len(settings.outputFile) > 0 {
				var err error
				file, err = os.OpenFile(settings.outputFile,
					os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
				if err != nil {
					log.Fatalf("failed to open file '%v': %v", settings.outputFile, err)
				}
				defer file.Close()
			}
			fmt.Fprint(file, proof)
		}
	}
	verifyCommand := func(args []string) {
		const verifyUsage = `sigsum verify PROOF [OPTIONS] < INPUT
    Options:
      -h --help Display this help
      --submit-key SUBMIT-KEY
      --log-key LOG-KEY

    Verifies a sigsum proof, as produced by sigsum leaf. Proof file specified on command line,
    data being verifier read from stdin.
`
		settings := parseVerifySettings(args, verifyUsage)
		submitKey := readPublicKeyFile(settings.submitKey)
		logKey := readPublicKeyFile(settings.logKey)

		// TODO: Optionally create message by hashing stdin.
		msg := readMessage(os.Stdin)

		// TODO: Could use a variant of ascii.Parser that treats empty line as EOF.
		proof, err := os.ReadFile(settings.proofFile)
		if err != nil {
			log.Fatalf("reading proof file %q failed: %v", settings.proofFile, err)
		}
		proofParts := bytes.Split(proof, []byte{'\n', '\n'})
		if len(proofParts) < 3 {
			log.Fatal("invalid proof, too few parts")
		}
		checkProofHeader(proofParts[0], &logKey)
		leafHash := checkProofLeaf(proofParts[1], msg[:], &submitKey)
		var cth types.CosignedTreeHead
		if err := cth.FromASCII(bytes.NewBuffer(proofParts[2])); err != nil {
			log.Fatal("failed to parse cosigned tree head: %v", err)
		}
		if !cth.VerifyLogSignature(&logKey) {
			log.Fatal("invalid log signature on tree head")
		}
		// TODO: Check cosignatures, process timestamp?

		if cth.TreeSize == 0 {
			log.Fatal("empty tree")
		}
		if cth.TreeSize == 1 {
			if len(proofParts) != 3 {
				log.Fatal("invalid proof, unexpected inclusion part for tree_size 1")
			}
			if cth.RootHash != leafHash {
				log.Fatal("inclusion check failed (for tree_size 1)")
			}
			return
		}
		if len(proofParts) != 4 {
			log.Fatal("invalid proof, got %d parts, need 4", len(proofParts))
		}
		var inclusion types.InclusionProof
		if err := inclusion.FromASCII(bytes.NewBuffer(proofParts[3]), cth.TreeSize); err != nil {
			log.Fatal("failed to parse inclusion proof: %v", err)
		}
		if err := merkle.VerifyInclusion(&leafHash, inclusion.LeafIndex, cth.TreeSize, &cth.RootHash, inclusion.Path); err != nil {
			log.Fatalf("inclusion proof invalid: %v", err)
		}
	}

	log.SetFlags(0)
	if len(os.Args) <= 1 {
		log.Printf(usage)
		os.Exit(1)
	}
	switch os.Args[1] {
	case "help":
		fmt.Printf(usage)
	case "key":
		keyCommand(os.Args[2:])
	case "leaf":
		leafCommand(os.Args[2:])
	case "verify":
		verifyCommand(os.Args[2:])
	default:
		log.Printf(usage)
	}
}

func parseKeyGenSettings(args []string) KeyGenSettings {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	outputFile := flags.String("o", "", "Output file")
	sshFormat := flags.Bool("ssh", false, "Use OpenSSH format for public key")

	flags.Parse(args)

	if len(*outputFile) == 0 {
		log.Printf("output file (-o option) missing")
		os.Exit(1)
	}
	return KeyGenSettings{*outputFile, *sshFormat}
}

func parseKeyVerifySettings(args []string) KeyVerifySettings {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	keyFile := flags.String("k", "", "Key file")
	signatureFile := flags.String("s", "", "Signature file")
	namespace := flags.String("n", "tree-head:v0@sigsum.org", "Signature namespace")

	flags.Parse(args)

	if len(*keyFile) == 0 {
		log.Printf("key file (-k option) missing")
		os.Exit(1)
	}
	if len(*signatureFile) == 0 {
		log.Printf("signature file (-s option) missing")
		os.Exit(1)
	}
	return KeyVerifySettings{
		keyFile:       *keyFile,
		signatureFile: *signatureFile,
		namespace:     *namespace,
	}
}

func parseKeySignSettings(args []string) KeySignSettings {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	keyFile := flags.String("k", "", "Key file")
	outputFile := flags.String("o", "", "Signature output file")
	namespace := flags.String("n", "tree-leaf:v0@sigsum.org", "Signature namespace")
	sshFormat := flags.Bool("ssh", false, "Use OpenSSH format for public key")

	flags.Parse(args)

	if len(*keyFile) == 0 {
		log.Fatalf("key file (-k option) missing")
	}
	return KeySignSettings{
		keyFile:    *keyFile,
		outputFile: *outputFile,
		namespace:  *namespace,
		sshFormat:  *sshFormat,
	}
}

func parseLeafSettings(args []string, usage string) LeafSettings {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	flags.Usage = func() { fmt.Print(usage) }

	keyFile := flags.String("k", "", "Key file")
	logUrl := flags.String("log-url", "", "Log base url")
	logKey := flags.String("log-key", "", "Public key file for log")
	outputFile := flags.String("o", "", "Output file")

	flags.Parse(args)
	if len(*logUrl) > 0 && len(*logKey) == 0 {
		log.Fatalf("--log-url option requires log's public key (--log-key option)")
	}
	return LeafSettings{
		keyFile:    *keyFile,
		logUrl:     *logUrl,
		logKey:     *logKey,
		outputFile: *outputFile,
	}
}

func parseVerifySettings(args []string, usage string) VerifySettings {
	flags := flag.NewFlagSet("", flag.ExitOnError)
	flags.Usage = func() { fmt.Print(usage) }

	submitKey := flags.String("submit-key", "", "Public key file")
	logKey := flags.String("log-key", "", "Public key file for log")

	flags.Parse(args)
	if flags.NArg() != 1 {
		log.Fatalf("no proof given on command line")
	}
	if len(*submitKey) == 0 {
		log.Fatalf("--submit-key argument is required")
	}
	if len(*logKey) == 0 {
		log.Fatalf("--log-key argument is required")
	}
	return VerifySettings{
		proofFile: flags.Arg(0),
		submitKey: *submitKey,
		logKey:    *logKey,
	}
}

func writeToFile(fileName string, data string, mode os.FileMode) {
	file, err := os.OpenFile(fileName,
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY, mode)
	if err != nil {
		log.Fatalf("failed to open file '%v': %v", fileName, err)
	}
	defer file.Close()
	_, err = fmt.Fprint(file, data)
	if err != nil {
		log.Fatalf("write failed to file '%v': %v", fileName, err)
	}
}

func writeKeyFile(outputFile string, sshFormat bool,
	pub *crypto.PublicKey, priv *crypto.PrivateKey) {
	writeToFile(outputFile, hex.EncodeToString(priv[:]), 0600)

	var serializedPub string
	if sshFormat {
		serializedPub = ssh.FormatPublicEd25519(pub[:])
	} else {
		serializedPub = hex.EncodeToString(pub[:])
	}
	// Openssh insists that also public key files have
	// restrictive permissions.
	writeToFile(outputFile+".pub", serializedPub, 0600)
}

func writeSignatureFile(outputFile string, sshFormat bool,
	public *crypto.PublicKey, namespace string, signature *crypto.Signature) {
	var serializedSignature string
	if sshFormat {
		serializedSignature = ssh.FormatSignature(
			public[:], namespace, signature[:])
	} else {
		serializedSignature = hex.EncodeToString(signature[:]) + "\n"
	}
	file := os.Stdout
	if len(outputFile) > 0 {
		var err error
		file, err = os.OpenFile(outputFile,
			os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("failed to open file '%v': %v", outputFile, err)
		}
		defer file.Close()
	}
	_, err := fmt.Fprint(file, serializedSignature)
	if err != nil {
		log.Fatalf("writing signature output failed: %v", err)
	}
}

func readPublicKeyFile(fileName string) crypto.PublicKey {
	contents, err := os.ReadFile(fileName)
	if err != nil {
		log.Fatalf("reading file %q failed: %v", fileName, err)
	}
	key, err := key.ParsePublicKey(string(contents))
	if err != nil {
		log.Fatalf("parsing file %q failed: %v", fileName, err)
	}
	return key
}

func readPrivateKeyFile(fileName string) crypto.Signer {
	contents, err := os.ReadFile(fileName)
	if err != nil {
		log.Fatalf("reading file %q failed: %v", fileName, err)
	}
	signer, err := local_key.ParsePrivateKeyFile(string(contents))
	if err != nil {
		log.Fatalf("parsing file %q failed: %v", fileName, err)
	}
	return signer
}

func readSignatureFile(fileName string,
	pub *crypto.PublicKey, namespace string) (ret crypto.Signature) {
	contents, err := os.ReadFile(fileName)
	if err != nil {
		log.Fatalf("reading file %q failed: %v", fileName, err)
	}
	ascii := strings.TrimLeft(string(contents), " \t")
	if strings.HasPrefix(ascii, "-----BEGIN") {
		signature, err := ssh.ParseSignatureFile(ascii, pub[:], namespace)
		if err != nil {
			log.Fatal(err)
		}
		copy(ret[:], signature)
		return
	}
	signature, err := hex.DecodeString(strings.TrimSpace(string(contents)))
	if err != nil {
		log.Fatal(err)
	}
	if len(signature) != 64 {
		log.Fatalf("invalid signature length %v", len(signature))
	}
	copy(ret[:], signature)
	return
}

func readMessage(r io.Reader) (ret crypto.Hash) {
	// One extra byte, to detect EOF.
	msg := make([]byte, 33)
	if readCount, err := io.ReadFull(os.Stdin, msg); err != io.ErrUnexpectedEOF || readCount != 32 {
		if err != nil && err != io.ErrUnexpectedEOF {
			log.Fatalf("reading message from stdin failed: %v", err)
		}
		log.Fatalf("sigsum message must be exactly 32 bytes, got %d", readCount)
	}
	copy(ret[:], msg)
	return
}

func submitLeaf(logUrl string, logKey *crypto.PublicKey, leaf *requests.Leaf) (string, error) {
	// We need the leaf hash.
	leafHash := leafHash(leaf)

	// TODO: should use sigsum-go's Client.AddLeaf, but that's not yet implemented.
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client := http.Client{}
	delay := 2 * time.Second

	for {
		addLeafUrl := types.EndpointAddLeaf.Path(logUrl)
		buf := bytes.Buffer{}
		leaf.ToASCII(&buf)

		req, err := http.NewRequestWithContext(ctx, http.MethodPost, addLeafUrl, &buf)
		if err != nil {
			return "", fmt.Errorf("creating the add-leaf request faild: %v", err)
		}
		resp, err := client.Do(req)
		if err != nil {
			// TODO: Should probably terminate on these errors.
			if err, ok := err.(*url.Error); ok && err.Timeout() {
				log.Fatalf("timed out: %v", err)
			}
			log.Printf("add-leaf request failed, will retry: %v", err)
			time.Sleep(delay)
			continue
		}
		// Don't care about body
		resp.Body.Close()

		if resp.StatusCode == http.StatusOK {
			break
		}
		time.Sleep(delay)
	}
	// Leaf submitted, now get a signed tree head + inclusion proof.
	for {
		getTreeHeadUrl := types.EndpointGetTreeHeadCosigned.Path(logUrl)
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, getTreeHeadUrl, nil)
		if err != nil {
			return "", fmt.Errorf("creating the get tree request faild: %v", err)
		}

		resp, err := client.Do(req)
		if err != nil {
			return "", fmt.Errorf("failed to get tree head: %v", err)
		}
		if resp.StatusCode != http.StatusOK {
			resp.Body.Close()
			return "", fmt.Errorf("get tree head gave response %q", resp.Status)
		}
		var cth types.CosignedTreeHead
		err = cth.FromASCII(resp.Body)
		resp.Body.Close()
		if err != nil {
			return "", fmt.Errorf("failed to parse tree head: %v", err)
		}
		if !cth.SignedTreeHead.VerifyLogSignature(logKey) {
			return "", fmt.Errorf("log's tree head signature is invalid: %v", err)
		}

		// See if we can have an inclusion proof for this tree size.
		if cth.TreeSize == 0 {
			// Certainly not included yet.
			time.Sleep(delay)
			continue
		}
		var proof types.InclusionProof
		// Special case for the very first leaf.
		if cth.TreeSize == 1 {
			if cth.RootHash != leafHash {
				// Certainly not included yet.
				time.Sleep(delay)
				continue
			}
			proof.TreeSize = 1
		} else {
			getInclusionProofUrl := fmt.Sprintf("%s/%d/%x",
				types.EndpointGetInclusionProof.Path(logUrl), cth.TreeSize, leafHash)
			req, err = http.NewRequestWithContext(ctx, http.MethodGet, getInclusionProofUrl, nil)
			if err != nil {
				return "", fmt.Errorf("creating the get-inclusion-proof request faild: %v", err)
			}
			resp, err = client.Do(req)
			if err != nil {
				return "", fmt.Errorf("failed to get inclusion proof: %v", err)
			}
			if resp.StatusCode != http.StatusOK {
				resp.Body.Close()
				log.Printf("no inclusion proof yet, will retry, will retry: %v", resp.Status)
				time.Sleep(delay)
				continue
			}
			err = proof.FromASCII(resp.Body, cth.TreeSize)
			resp.Body.Close()
			if err != nil {
				return "", fmt.Errorf("failed to parse inclusion proof: %v", err)
			}
		}

		// Check validity.
		if err = merkle.VerifyInclusion(&leafHash, proof.LeafIndex, cth.TreeSize, &cth.RootHash, proof.Path); err != nil {
			return "", fmt.Errorf("inclusion proof invalid: %v", err)
		}

		// Output collected data.
		buf := bytes.Buffer{}

		fmt.Fprintf(&buf, "version=0\nlog=%x\n\n", crypto.HashBytes(logKey[:]))

		fmt.Fprintf(&buf, "leaf=%x %x\n\n", crypto.HashBytes(leaf.PublicKey[:]), leaf.Signature)

		cth.ToASCII(&buf)

		if cth.TreeSize > 1 {
			fmt.Fprintf(&buf, "\n")
			proof.ToASCII(&buf)
		}
		return string(buf.Bytes()), nil
	}
}

func checkProofHeader(header []byte, logKey *crypto.PublicKey) {
	p := ascii.NewParser(bytes.NewBuffer(header))
	if version, err := p.GetInt("version"); err != nil || version != 0 {
		if err != nil {
			log.Fatalf("invalid version line: %v", err)
		}
		log.Fatalf("unexpected version %d, wanted 0")
	}
	if hash, err := p.GetHash("log"); err != nil || hash != crypto.HashBytes(logKey[:]) {
		if err != nil {
			log.Fatalf("invalid log line: %v", err)
		}
		log.Fatalf("proof doesn't match log's public key")
	}
	if err := p.GetEOF(); err != nil {
		log.Fatalf("invalid proof header: %v", err)
	}

}

// On success, returns the leaf hash.
func checkProofLeaf(leaf []byte, msg []byte, submitKey *crypto.PublicKey) crypto.Hash {
	p := ascii.NewParser(bytes.NewBuffer(leaf))
	values, err := p.GetValues("leaf", 2)
	if err != nil {
		log.Fatalf("invalid leaf line: %v", err)
	}
	keyHash, err := crypto.HashFromHex(values[0])
	if err != nil || keyHash != crypto.HashBytes(submitKey[:]) {
		log.Fatalf("unexpected leaf key hash: %q", values[0])
	}
	signature, err := crypto.SignatureFromHex(values[1])
	if err != nil {
		log.Fatalf("failed to perse signature: %v", err)
	}
	if !types.VerifyLeafMessage(submitKey, msg, &signature) {
		log.Fatalf("leaf signature not valid")
	}
	return merkle.HashLeafNode((&types.Leaf{
		Checksum:  crypto.HashBytes(msg[:]),
		KeyHash:   keyHash,
		Signature: signature,
	}).ToBinary())
}

// TODO: There should be some library utility for this.
func leafHash(leaf *requests.Leaf) crypto.Hash {
	return merkle.HashLeafNode((&types.Leaf{
		Checksum:  crypto.HashBytes(leaf.Message[:]),
		KeyHash:   crypto.HashBytes(leaf.PublicKey[:]),
		Signature: leaf.Signature,
	}).ToBinary())
}
