module key

go 1.18

require sigsum.org/sigsum-go v0.1.6-0.20221124110514-431a5bccf7ff

require local/ssh v0.0.0

replace local/ssh => ../ssh
