package key

import (
	"bytes"
	"fmt"
	"strings"

	"local/ssh"

	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/key"
)

// Supports multiple input formats:
// * 32-byte hex encoded private key  TODO: Consider addign private: prefix
// * Openssh-formatted private key
// * Openssh-formatted public key
// In case only the public key is available,
// attempt to use ssh-agent to access the public key.
func ParsePrivateKeyFile(ascii string) (crypto.Signer, error) {
	if strings.HasPrefix(ascii, "-----BEGIN") {
		keys, err := ssh.ParsePrivateEd25519File(ascii)
		if err != nil {
			return nil, err
		}
		var private crypto.PrivateKey
		copy(private[:], keys[:32])
		signer := crypto.NewEd25519Signer(&private)
		public := signer.Public()
		if !bytes.Equal(public[:], keys[32:]) {
			return nil, fmt.Errorf("inconsistent private and public key")
		}
		return signer, nil
	}
	return key.ParsePrivateKey(ascii)
}
