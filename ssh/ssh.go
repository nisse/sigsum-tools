package ssh

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"log"
	"strings"
)

// PEM utilities

func pemBegin(name string) string {
	return "-----BEGIN " + name + "-----\n"
}
func pemEnd(name string) string {
	return "\n-----END " + name + "-----\n"
}

// Decode a base64 file with PEM-style BEGIN and END markers
func base64PemDecode(ascii, name string) ([]byte, error) {
	// TODO: Search for begin and end marker, in particular, allow
	// additional empty lines.
	prefix := pemBegin(name)
	suffix := pemEnd(name)
	if !strings.HasPrefix(ascii, prefix) ||
		!strings.HasSuffix(ascii, suffix) {
		return nil, fmt.Errorf("invalid PEM encapsulation of: %v", name)
	}
	blob, err := base64.StdEncoding.DecodeString(ascii[len(prefix) : len(ascii)-len(suffix)])
	if err != nil {
		return nil, err
	}
	return blob, nil
}

func base64PemEncode(blob []byte, name string) string {
	return pemBegin(name) +
		base64.StdEncoding.EncodeToString(blob) +
		pemEnd(name)
}

// Utilities to construct SSH messages.

func join(l ...[]byte) []byte {
	return bytes.Join(l, nil)
}

func serializeUint32(x uint32) []byte {
	buffer := make([]byte, 4)
	binary.BigEndian.PutUint32(buffer, x)
	return buffer
}

func serializeString(data []byte) []byte {
	if int64(len(data)) > int64(^uint32(0)) {
		log.Panicf("string too large for ssh, length %d", len(data))
	}
	buffer := make([]byte, 4+len(data))
	binary.BigEndian.PutUint32(buffer, uint32(len(data)))
	copy(buffer[4:], data)
	return buffer
}

// Exported for ssh-agent
var Uint32 = serializeUint32
var String = serializeString

// Utilities to parse SSH messages.

// Skips prefix, if present, otherwise return nil.
func skipPrefix(buffer []byte, prefix []byte) []byte {
	if !bytes.HasPrefix(buffer, prefix) {
		return nil
	}
	return buffer[len(prefix):]
}

// Skips an ssh-encoded string, uncluding length field.
func skipPrefixString(buffer []byte, prefix []byte) []byte {
	return skipPrefix(buffer, serializeString(prefix))
}

func parseUint32(buffer []byte) (uint32, []byte) {
	if buffer == nil || len(buffer) < 4 {
		return 0, nil
	}
	return binary.BigEndian.Uint32(buffer[:4]), buffer[4:]
}

func parseString(buffer []byte) ([]byte, []byte) {
	length, buffer := parseUint32(buffer)
	if buffer == nil {
		return nil, nil
	}
	if int64(len(buffer)) < int64(length) {
		return nil, nil
	}
	return buffer[:int(length)], buffer[int(length):]
}

// Handling SSH public keys.

func SerializePublicEd25519(pub []byte) []byte {
	if len(pub) != 32 {
		log.Panicf("invalid ed25519 public key, got size %d", len(pub))
	}
	return join(serializeString([]byte("ssh-ed25519")), serializeString(pub))
}

func FormatPublicEd25519(pub []byte) string {
	return "ssh-ed25519 " +
		base64.StdEncoding.EncodeToString(SerializePublicEd25519(pub)) +
		" sigsum key\n"
}

func ParsePublicEd25519(asciiKey string) ([]byte, error) {
	// Split into fields, recognizing exclusively ascii space and TAB
	fields := strings.FieldsFunc(asciiKey, func(c rune) bool {
		return c == ' ' || c == '\t'
	})
	if len(fields) < 2 {
		return nil, fmt.Errorf("invalid public key, splitting line failed")
	}
	if fields[0] != "ssh-ed25519" {
		return nil, fmt.Errorf("unsupported public key type: %v", fields[0])
	}
	blob, err := base64.StdEncoding.DecodeString(fields[1])
	if err != nil {
		return nil, err
	}
	pub := skipPrefix(blob, join(serializeString([]byte("ssh-ed25519")), serializeUint32(32)))

	if pub == nil {
		return nil, fmt.Errorf("invalid public key blob prefix")
	}
	if len(pub) != 32 {
		return nil, fmt.Errorf("invalid public key length: %v", len(blob))
	}
	// Short and exclusively owned, no need to copy.
	return pub, nil
}

// Handling OpenSSH private keys.
//
// The key format is somewhat undocumented. This serialization is
// based on the description at
// https://coolaj86.com/articles/the-openssh-private-key-format/

var opensshPrivateKeyPrefix = join(
	[]byte("openssh-key-v1"), []byte{0},
	// cipher "none", kdf "none"
	serializeString([]byte("none")), serializeString([]byte("none")),
	serializeUint32(0), serializeUint32(1)) // empty kdf, and #keys = 1

func FormatPrivateEd25519(priv []byte, pub []byte) string {
	if len(priv) != 32 {
		panic(fmt.Errorf("invalid ed25519 private key, got size %d",
			len(priv)))
	}

	pubBlob := SerializePublicEd25519(pub)

	nonce := make([]byte, 4)
	_, err := rand.Read(nonce)
	if err != nil {
		log.Fatal(err)
	}

	blob := join(
		// Prefix + first copy of public key
		opensshPrivateKeyPrefix, serializeString(pubBlob),

		// Followed by the data that could be encrypted, but isn't in our case.
		// Length of below data.
		serializeUint32(136),
		// Add nonce twice, presumably to check for correct decryption
		nonce, nonce,

		// Private key is public key + additional private parameters.
		pubBlob,

		// Finally, the ssh secret key, which includes the raw public
		// key once more.
		serializeUint32(64), // Length of private + public key
		priv,
		pub,
		// Empty comment.
		serializeUint32(0),
		// Padding
		[]byte{1, 2, 3, 4, 5})

	// Size of above is
	//   8 (nonce)
	//  51 (public part)
	//  68 (private part)
	//   4 (comment)
	//   5 (padding)
	// ----
	// 136 (sum)

	// Then convert to base 64 and add header and footer
	return base64PemEncode(blob, "OPENSSH PRIVATE KEY")
}

// Returned value consists of the 32-byte private key + 32 byte public
// key.
func ParsePrivateEd25519(blob []byte) ([]byte, error) {
	blob = skipPrefix(blob, opensshPrivateKeyPrefix)
	if blob == nil {
		return nil, fmt.Errorf("invalid or encrypted private key")
	}
	publicKeyBlob, blob := parseString(blob)
	if blob == nil {
		return nil, fmt.Errorf("invalid private key, pubkey missing")
	}
	// TODO: Sanity check public key.
	length, blob := parseUint32(blob)
	if blob == nil || int64(length) != int64(len(blob)) ||
		length%8 != 0 {
		return nil, fmt.Errorf("invalid private key")
	}
	n1, blob := parseUint32(blob)
	n2, blob := parseUint32(blob)
	if blob == nil || n1 != n2 {
		return nil, fmt.Errorf("invalid private key, bad nonce")
	}
	blob = skipPrefix(blob, publicKeyBlob)
	if blob == nil {
		return nil, fmt.Errorf("invalid private key, inconsistent publioc key")
	}
	keys, blob := parseString(blob)
	if blob == nil {
		return nil, fmt.Errorf("invalid private key, private key missing")
	}
	if len(keys) != 64 {
		return nil, fmt.Errorf("unexpected private key size: %d", len(keys))
	}
	// TODO: Check consistency with preceding public key.
	// Return private and public halves.
	return keys, nil
}

func ParsePrivateEd25519File(ascii string) ([]byte, error) {
	blob, err := base64PemDecode(ascii, "OPENSSH PRIVATE KEY")
	if err != nil {
		return nil, err
	}
	return ParsePrivateEd25519(blob)
}

// OpenSSH signatures.

func SignedDataFromHash(namespace string, hash [sha256.Size]byte) []byte {
	return join([]byte("SSHSIG"),
		serializeString([]byte(namespace)),
		serializeUint32(0), // Empty reserved string
		serializeString([]byte("sha256")),
		serializeString(hash[:]))
}

func SignedData(namespace string, msg []byte) []byte {
	return SignedDataFromHash(namespace, sha256.Sum256(msg))
}

func SerializeSignature(publicKey []byte, namespace string, signature []byte) []byte {
	if len(signature) != 64 {
		log.Panicf("invalid ed25519 signature, got size %d", len(signature))
	}

	return join(
		[]byte("SSHSIG"),
		serializeUint32(1), // version 1
		serializeString(SerializePublicEd25519(publicKey)),
		serializeString([]byte(namespace)),
		serializeUint32(0), // Empty reserved string
		serializeString([]byte("sha256")),
		serializeUint32(83),
		serializeString([]byte("ssh-ed25519")),
		serializeString(signature))
}
func FormatSignature(publicKey []byte, namespace string, signature []byte) string {
	return base64PemEncode(SerializeSignature(publicKey, namespace, signature),
		"SSH SIGNATURE")
}

func ParseSignature(blob []byte) ([]byte, error) {
	signature := skipPrefix(blob, join(
		serializeUint32(83), // length of signature
		serializeString([]byte("ssh-ed25519")),
		serializeUint32(64)))
	if signature == nil {
		return nil, fmt.Errorf("invalid signature blob")
	}
	if len(signature) != 64 {
		return nil, fmt.Errorf("bad signature length: %d", len(signature))
	}
	// Short and exclusively owned, no need to copy.
	return signature, nil
}

func ParseSignatureFile(ascii string, publicKey []byte, namespace string) ([]byte, error) {
	blob, err := base64PemDecode(ascii, "SSH SIGNATURE")
	if err != nil {
		return nil, err
	}

	blob = skipPrefix(blob, join(
		[]byte("SSHSIG"),
		serializeUint32(1))) // version 1

	if blob == nil {
		return nil, fmt.Errorf("invalid signature prefix")
	}
	blob = skipPrefixString(blob, SerializePublicEd25519(publicKey))
	if blob == nil {
		return nil, fmt.Errorf("signature public key not as expected")
	}
	blob = skipPrefixString(blob, []byte(namespace))
	if blob == nil {
		return nil, fmt.Errorf("signature namespace not as expected")
	}
	blob = skipPrefix(blob, join(
		serializeUint32(0), // Empty reserved string
		serializeString([]byte("sha256"))))
	if blob == nil {
		return nil, fmt.Errorf("signature hash not as expected")
	}
	return ParseSignature(blob)
}
