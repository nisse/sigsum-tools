all:
	cd sigsum && $(MAKE)

check: all
	cd testsuite && $(MAKE) check

.PHONY: all check
